#include<stdio.h>

const int MAX = 3;

int main()
{
    int var[] = {10, 100, 200};
    int i, *ptr;

    //指针第一个元素的地址
    ptr = var;
    i = 0;

    while (ptr <= &var[MAX - 1])
    {
        printf("Adress of var[%d] = %ls\n", i, ptr);
        printf("Value of var[%d] = %d\n", i, *ptr);

        ptr++;
        i++;
    }

    return 0;
}