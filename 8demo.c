#include<stdio.h>

int main()
{
    int var = 20;//实际变量的声明
    int *ip;

    ip = &var;

    printf("Adress of var variable: %p\n", &var);

    //在指针中存储变量的地址
    printf("Address stored in ip variable: %p\n", ip);

    //使用指针访问值
    printf("value of *ip variable: %d\n", *ip);

    return 0;
}