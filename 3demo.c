/*多维数组
c语言支持多维数组。多维数组的声明的一般形式如下
type name[size1][size2]...[sizeN];

二维数组
多维数组最简单的形式是二维数组。一个二维数组，再本质上，是一个数组列表。
声明一个x行y列的二维数组格式:type arrayName [ x ][ y ];

*/

#include<stdio.h>

int main()
{
    //一个带有5行2列的数组
    int a[5][2] = {{0,0}, {2,4}, {3,6}, {4.8}};
    int i, j;

    for (i = 0; i < 5; i++){
        for (j = 0; j < 2; j++){
            printf("a[%d][%d] = %d\n", i, j, a[i][j]);
        }
    }

    return 0;
}