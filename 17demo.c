#include<stdio.h>
#include<string.h>

int main()
{
    char str1[12] = "Hello";
    char str2[12] = "World";
    char str3[12];
    int len;

    //复制str1到str3
    strcpy(str1, str3);

    //连接str1和str2
    strcat(str1, str2);

    //连接后str1的总长度
    len = strlen(str1);
    printf("strlen(str1): %d\n", len);

    return 0;
}