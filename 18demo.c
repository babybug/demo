#include<stdio.h>
#include<string.h>

struct Books
{
    char title[50];
    char author[50];
    char subject[50];
    int books_id;
};

//函数声明
void printbook(struct Books book);

int main()
{
    struct Books Book1;
    struct Books Book2;

    //Book1描述
    strcpy(Book1.title, "C Prigram");
    strcpy(Book1.author, "Nuha Ali");
    strcpy(Book1.subject, "C Prigram Tutorial");
    Book1.books_id = 6495407;
    
    //Book2描述
    strcpy(Book2.title, "Telecom Billing");
    strcpy(Book2.author, "Zara Ali");
    strcpy(Book2.subject, "Telecom Billing Tutorial");
    Book2.books_id = 649700;

    //输出Book1的信息
    printbook(Book1);

    //输出Book2的信息
    printbook(Book2);

    return 0;
}

void printbook(struct Books book)
{
    printf("Book title: %s\n", book.title);
    printf("Book author: %s\n", book.author);
    printf("Book subject: %s\n", book.subject);
    printf("Book book_id: %d\n", book.books_id);
}