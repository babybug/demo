/*枚举
enum　枚举名　{枚举元素1,枚举元素2,……};
*/

#include<stdio.h>

// enum DAY{
//     MON=1, TUE, WED, THU, FRI, SAT, SUN
// };

// int main()
// {
//     enum DAY day;
//     day = WED;
//     printf("%d\n", day);

//     return 0;
//}

enum DAY{
    MON=1, TUE, WED, THU, FRI, SAT, SUN
} day;

int main()
{
    for (day = MON; day <= SUN; day++){
        printf("枚举元素：%d＼ｎ", day);
    }

    return 0;
}