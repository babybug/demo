/*声明数组type arrayName [ arraySize ];
一维数组arrysize必须十一个大于零的常量，
type可以是任意有效的c数据类型。
初始化数组
访问数组元素
double salary = balance[9];
*/
#include<stdio.h>

int main()
{
    int n[10];//一个包含10个整数的数组
    int i, j;

    //初始化数组
    for (i = 0; i < 10; i++){
        n[i] = i + 100;//设置i为i+100
    }
    //输出数组中每个元素的值
    for (j = 0; j <10; j++){
        printf("Element[%d = %d\n", j, n[j]);
    }

    return 0;
}