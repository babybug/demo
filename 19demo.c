#include<stdio.h>
#include<string.h>

struct Books
{
    char title[50];
    char author[50];
    char subject[50];
    int book_id;
};
//声明函数
void printbook(struct Books *book);

int main()
{
    struct Books Book1;
    struct Books Book2;

    //Book1描述
    strcpy(Book1.title, "C Program");
    strcpy(Book1.author,"Nhua Ali");
    strcpy(Book1.subject, "C Program Tutorial");
    Book1.book_id = 6495407;
    
    //Book2描述
    strcpy(Book2.title, "Telecom Billing");
    strcpy(Book2.author,"Zara Ali");
    strcpy(Book2.subject, "Telecom billing Tutorial");
    Book2.book_id = 6495700;
    //调用函数
    
    //打印book1信息
    printbook(&Book1);
    //打印book2信息
    printbook(&Book2);

    return 0;
}
//定义函数
void printbook(struct Books *book)
{
    printf("Book title: %s\n", book->title);
    printf("Book author: %s\n", book->author);
    printf("Book subject: %s\n", book->subject);
    printf("Book book_id: %d\n", book->book_id);
}